// const CryptoJS = require("crypto-js");
// const SHA256 = require("crypto-js/sha256");

var path = require('path');
var express = require('express');
var app = express();
app.use(express.static(path.join(__dirname, 'utils')));
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
const Global = require("../utils/Global.js");
let BASE_PATH = (__dirname.split('\\bin')[0]).split('/bin')[0];
let users = [];
let connections = [];
const rooms = [];

server.listen(process.env.PORT || 3000);
console.log('server is running ...')

io.sockets.on('connection', function(socket) {
  const axios = Global.instanceAxios;
  const userId = socket.handshake.query.userId;
  users[userId] = socket;
  connections.push(socket);
  console.log(socket.handshake.address)
  console.log("NBR connecter: "+ connections.length)

  socket.on('first-connection', function(data) {
    console.log(data);
    axios.defaults.headers.common["x-access-token"] = data.token;
    socket.emit("first-connection-emit")
  });

  socket.on('disconnect', function (data) {
    connections.splice(connections.indexOf(socket), 1);
  });

  socket.on('new-message', function (data) {
    axios.defaults.headers.common["x-access-token"] = data.token;
    axios.get("chat/conversationsByUserId", {}).then(({data: response}) => {
      console.log(response)
      io.sockets.emit('new-message-emit', {success: true, data: response.data});
    }).catch((errors) => {
      io.sockets.emit('new-message-emit', {success: false, code: "error-add-message", error: errors});
    });
    delete data.token;
    axios.post("chat/add-message", data, {}).then(({data: response}) => {
      console.log(response)
      io.sockets.emit('new-message-emit', {success: true, data: response.data});
    }).catch((errors) => {
      io.sockets.emit('new-message-emit', {success: false, code: "error-add-message", error: errors}); 
    });
  });

  socket.on('test', function (data) {
    console.log(socket.handshake.query);
  });
});


