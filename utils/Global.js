const axios = require("axios");
const config = require("../config/config.js");

module.exports = {
  instanceAxios: axios.create({
    baseURL: config.backendEndPoint,
  })
};
